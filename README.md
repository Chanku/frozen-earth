Frozen Earth Engine
===================
Introduction
--------------
This is Frozen Earth (or the Frozen Earth Engine). It's a simple 2D Game Engine built entirely in Python. It includes many features, including a Lisp Dialect, FEEL (Frozen Earth Engine Lisp), in which most scripting can occur in.

Features
--------
1. Lisp-Based Scripting Language (FEEL). FEEL is directly translated to Python 3, so it can leverage python as well!
2. Has a decent amount of the harder-code already done
3. Can also run Python Code

PFAQ
----
#####Why should I use this?  
If you are better with a better engine then use that instead. I have no issues with accepting that. However if you just want to try this engine you can.

#####Why not just license under the LGPL v2.1 or MIT?  
Because I was torn between the two, and I really don't want this to be closed, but at the same time I don't want to prevent people from being able to build off of it without the license infecting them.

Also because of the GPL's infectious nature, there is the possibility of the GPL infecting the translated FEEL code. 

#####Why Python 3?  
It's the only thing I really know, and I wanted to try out making a game engine.

#####Why do you not have a Code of Conduct?    
Because personally I see them as unnecessary. I will accept code from anyone so long as the code is good and follows the guidelines.

License
--------------
You may use this engine Non-Commerically you may use either the MIT or LGPL v2.1 License, if you use this Commercially you must use the LGPL v2.1 license. See the LICENSE file for the MIT License or the LICENSE.lesser for the LGPL License

(All Artwork provided with this is licensed under the CC-BY-SA)
Artwork License: [![CC-BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
