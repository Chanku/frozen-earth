CONTRIBUTING
============
    This document is designed to outline Contributing basics and how you can contribute to this project. This file is mainly for new contributors, but may also be useful for old Contributors. 

Table of Contents
-----------------
1. Getting Started
2. Structure
3. Additional Reading
4. Licensing
5. Code of Conduct 

1.Getting Started
---------------
    When getting started you first may see a bunch of issues or a lot of code you may not understand, if so then feel free to ask on our IRC channel (which can be found in the README.md file). Now there are a few things here that you must know, among the way our licensing works, the way that contributions are handled and so on and so forth. 

2.Structure
-----------
    The Structure of the project is currently as follows. There is the Benevolent Chancellor for Life (BCFL), and that is me. The BCFL can choose whatever to do with the project and has the final say on all matters. The BCFL may also choose a successor at his/her leisure. The BFCL's powers only truely extend to this repository and any forks do not have to abide by it's rules, however if you wish to have a successful merge it is suggested that you do so.  The BCFL may also choose the change this in the future. 

3.Additional Reading
------------------
    There are a few other documents you should consider reading, all of those documents are in the documents directory/folder. Especially the following
+ The Versioning Document
+ The Release/Development Cycle Document
+ The Deprecation Document
+ The Organization Document
+ The Code Guidelines Document

4.Licensing
---------
    You may have noticed that we provide both the LGPL v2.1 and the MIT License. You may use either license for modification on this project for non-profit uses, but for any uses that are not non-profit you must use the LGPL v2.1. However there is also an additional note if you offer it back to the main repository it must be dual-licensed under the same terms as us. If you change the license for your own personal usage, you may. HOWEVER when you offer any changes upstream you must offer them under those two licenses. However this only applies to code. You also must grant the repository with the ability for the project to change the licenses in the future without having to ask for your permission first. 

    For Artwork or Music or the like any modifications must follow the license of what it states in the README.md file. Otherwise you must grant them under the CC-BY or CC-BY-SA license for it to be accepted. If you do not use either of those, but use a license that allow free-redistribution and modification then please link the license and it may or may not be accepted. In some instances, even if you use a license that similar to the CC-BY or CC-BY-SA you may be asked to offer it under the CC-BY or CC-BY-SA instead. If that is the case, it may very well be rejected if you say no.

5.Code of Conduct
---------------
    While addressed by README.md file briefly, I will say this here. At this time no Code of Conduct is apart of this engine. However if we do adopt one in the future it will probably be the No Code of Conduct, Code of Merit, or something like the Ruby Code of Conduct. That is all I will be considering at this time. Any contributions will have to be offered with no Code of Conduct attached, or with the Code of Conduct the project has accepted at the time. If you stipulate any Code of Conduct other than what I have stated above, then it will be rejected. By contributing you also agree to a change in the Code of Conduct at any time, when the BCFL chooses.
