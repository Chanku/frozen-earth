#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import pygame
import main
import time
import game_basics

class Game():
    def __init__(self, levels, screen, player):
        self.levels = levels
        self.level = levels[0]
        self.move_to_next = False
        self.screen = screen
        self.level.load(self.screen)
        self.movement = 1
        self.player = player
        self.is_running = True
        self.attack = False
        self.mini_boss_alive = True

    def update(self):
        if self.attack:
            self.level.attack(self.player, self)
        self.level.update(self.screen)
        self.player.update() 
        self.level.collision_check(self.player, self) 
        self.attack = False

    def change_room(self):
        if self.move_to_next:
            self.level.unload()
            self.level = self.levels[self.levels.index(self.level) + self.movement]
            self.level.load(self.screen)
            self.move_to_next = False

class Level():
    def __init__(self, name):
        self.move_to_next = False
        self.grid = pygame.sprite.Group()
        self.detect_collision = False
        self.name = name

    def append(self, coord_tuple, obj):
        obj.set_pos(coord_tuple[0], coord_tuple[1])
        self.grid.add(obj)

    def add(self, coord_tuple, obj):
        self.append(cord_tuple, obj)

    def remove(self, obj):
        self.grid.kill(ob)

    def load(self, screen):
        self.grid.draw(screen)
        self.detect_collision = True

    def update(self, screen):
        self.grid.draw(screen)
        for thing in self.grid.sprites():
            if (thing.rect.x > main.height or thing.rect.x < 0) or (thing.rect.y > main.width or thing.rect.y < 0):
                thing.kill()

    def unload(self):
        self.detect_collision = False

    #Rewrite attack code soon
    def attack(self, player, game_instance):
        if 'up' in player.animation_string:
            player.change_y -= player.reach * player.speed
        elif 'down' in player.animation_string:
            player.change_y = player.reach * player.speed
        elif 'right' in player.animation_string:
            player.change_x = player.reach * player.speed
        elif 'left' in player.animation_string:
            player.change_x -= player.reach * player.speed
        player.move()
        has_enemies = False
        self.update(game_instance.screen)
        for thing in self.grid.sprites():
            if isinstance(thing, game_basics.Enemy):
                if thing.rect.colliderect(player) and thing.awaken:
                    self.undo_movement(thing)
                    self.undo_movement(thing)
                    self.undo_movement(thing)
                    thing.health -= player.damage
                    time.sleep(0.05)
                has_enemies = True
            elif isinstance(thing, game_basics.Destructable_Container):
                if thing.rect.colliderect(player):
                    thing.contains.set_pos(thing.rect.x, thing.rect.y)
                    self.grid.add(thing.contains)
                    thing.kill()
            elif isinstance(thing, game_basics.Enemy_Projectile):
                player.health -= thing.damage
                thing.kill()
        if not has_enemies:
            try:
                self.spawn_enemies()
            except AttributeError:
                pass
        self.undo_movement(player)
        player.change_x = 0
        player.change_y = 0
        game_instance.attack = False

    def check_player_collision(self, player, game_instance, thing):
        if isinstance(thing, game_basics.Terrain):
            if thing.collision:
                player.move()
                if thing.rect.colliderect(player):
                    self.test_movement(player)
                else:
                    self.undo_movement(player)
        elif isinstance(thing, game_basics.Lock):
            if thing.collision:
                pass
        elif isinstance(thing, game_basics.Room_Exit):
            if not thing.collision:
                if thing.rect.colliderect(player):
                    thing.move_mob(player)
                    game_instance.movement = thing.room_movement
                    game_instance.move_to_next = True
            elif thing.collision:
                player.move()
                if thing.rect.colliderect(player):
                    self.test_movement(player)
                else:
                    player.undo_movement(player)

        elif isinstance(thing, game_basics.Enemy):
            player.move()
            # if not isinstance(thing, mobs.Boss1) or not isinstance(thing, mobs.Boss2):
            #     if thing.rect.colliderect(player):
            #         if thing.awaken:
            #             self.test_movement(player)
            #             self.undo_movement(player)
            #             self.undo_movement(player)
            #             player.health -= thing.damage
            #             thing.change_x = 20
            #             thing.change_y = 20
            #             self.test_movement(thing)
            #             self.undo_movement(thing)
            #             thing.collision_counter = 2
            #         elif not thing.awaken:
            #             self.test_movement(player)
            #             self.undo_movement(player)
            #             self.undo_movement(player)
            #             thing.awaken = True
            #             print('awake!')
            self.undo_movement(player)
        # elif isinstance(thing, item.Door):
        #     if thing.rect.colliderect(player):
        #         if not thing.is_locked:
        #             if thing.x_change:
        #                 player.set_pos(thing.x_change, player.rect.y)
        #             if thing.y_change:
        #                 player.set_pos(player.rect.x, thing.y_change)
        #             game_instance.move_to_next = True
        #             game_instance.movement = thing.movement
        #         elif thing.is_locked:
        #             if 'key:{}'.format(thing.key_name) in player.inventory:
        #                 player.inventory.remove('key:{}'.format(thing.key_name))
        #                 if thing.x_change:
        #                     player.set_pos(thing.x_change, player.rect.y)
        #                 if thing.y_change:
        #                     player.set_pos(player.rect.x, thing.y_change)
        #                 game_instance.move_to_next = True
        #                 thing.is_locked = False
        #                 game_instance.movement = thing.movement
        #             else:
        #                 self.test_movement(player)

    def enemy_collision_check(self, thing, game_instance):
        for obj in self.grid.sprites():
            if thing != obj:
                if isinstance(thing, game_basics.Terrain):
                    if thing.collide:
                        player.move()
                        if thing.rect.colliderect(player):
                            self.test_movement(player)
                        else:
                            self.undo_movement(player)
                elif isinstance(thing, game_basics.Player):
                    pass
                elif isinstance(thing, game_basics.Player_Projectile):
                    thing.health -= obj.damage
                    obj.kill()

    def collision_check(self, player, game_instance):
        if self.detect_collision:
            for thing in self.grid.sprites():
                self.check_player_collision(player, game_instance, thing)
                if isinstance(thing, game_basics.Enemy):
                    thing.update(player)
                    self.enemy_collision_check(thing, game_instance)

###########################################################################################################################################
###########################################################################################################################################

#                                                        #
#                         NOTE                           #
# I have no idea wtf this code is doing or why it works. #
# I should really figure it out...                       #
# ~ Chandler L. Wise                                     #
#                                                        #

    def test_movement(self, player):
        if player.change_x != 0 and player.change_y == 0:
            self.undo_movement(player)
            self.undo_movement(player)
            self.change_x = 0
        elif player.change_x == 0 and player.change_y != 0:
            self.undo_movement(player)
            self.undo_movement(player)
            self.change_y = 0
        elif player.change_x != 0 and player.change_y != 0:
            self.undo_movement(player)
            self.undo_movement(player)
            player.change_y = 0
            player.change_x = 0

    def undo_movement(self, player):
        player.change_y = -1 * player.change_y
        player.change_x = -1 * player.change_x
        player.move()
        player.change_y = -1 * player.change_y
        player.change_x = -1 * player.change_x
