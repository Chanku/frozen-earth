#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import pygame
import game
import audio
import game_basics
import main
import sys

def intro_screen(screen, clock, fps=60):
    splash = pygame.image.load('./assets/splash.png')
    splash = pygame.transform.scale(splash, (main.HEIGHT, main.WIDTH)).convert()
    fade_in(splash, screen, fps, clock)
    load_audio = audio.load_audio()
    player = game_basics.Player(32,600-62)
    game_world = game.create_levels(screen, player)
    g1 = pygame.sprite.Group(player)
    game_world.player = player
    fade_out(splash, screen, fps, clock)
    return game_world, g1, player, load_audio

def main_menu(screen, clock, fps=320):
    pass

def check_for_quit():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)

def fade_in(image, screen, fps, clock):
    for i in range(255):
        check_for_quit()
        screen.fill((0,0,0))
        image.set_alpha(i)
        screen.blit(image, (0,0))
        pygame.display.flip()
        clock.tick(fps)

def fade_out(image, screen, fps, clock):
    for i in range(255):
        check_for_quit()
        screen.fill((0,0,0))
        image.set_alpha(255 - i)
        screen.blit(image, (0,0))
        pygame.display.flip()
        clock.tick(fps)
