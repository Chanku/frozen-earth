#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import sys
import math
import os
import operator as op
import main
import global_values
import multiprocessing
allow_livepatching = global_values.ALLOW_LIVEPATCHING
inline_python = True
try:
    if main.DEBUG and allow_livepatching:
        READ_PROCESS = None
        READ_PIPE = None
except AttributeError:
    pass


python_program = (#'import math\nimport world_script\nimport operator as op\nimport game_basics\ncons=lambda x, y: [x] + y\n'
                  # 'import math\nimport operator as op\nglobal world_script, main, game_basics\nncons=lambda x, y: [x] + y\n'
                  'import math\nimport operator as op\nncons=lambda x, y: [x] + y\n'
                  'car=lambda x: x[0]\ncdr=lambda x: x[1:]\nfunctionp=lambda x: hasattr("__call__")\n'
                  'gObjectp=lambda x:hasattr("set_pos")\nnullp=lambda x: x == []\n'
                  'def delfunc(str_x):\n'
                  ' if eval("hasattr({}, \'__call__\')".format(str_x)) and not eval("hasattr({},\'set_pos\')".format(str_x)):\n'
                  '  exec("global {};del {}".format(str_x, str_x))\n'
                  ' else:\n'
                  '  print("Attempt to use delfunc on a non-function! Ignoring!")\n'
                  'def delgObject(str_x):\n'
                  ' if eval("hasattr({}, \'__call__\')".format(str_x)) and eval("hasattr({},\'set_pos\')".format(str_x)):\n'
                  '  exec("global {};del {}".format(str_x, str_x))\n'
                  ' else:\n'
                  '  print("Attempt to use delgObject on a non-gObject! Ignoring!")\n'
                  'def delvar(str_x):\n'
                  ' if not eval("hasattr({}, \'__call__\')".format(str_x)) and not eval("hasattr({}, \'set_pos\')".format(str_x)):\n'
                  '  exec("global {};del {}".format(str_x, str_x))\n'
                  ' else:\n'
                  '  print("Attempt to use delvar on a non-var! Ignoring!")\n'
                  'def live_patch(level_num, new_code, regen_from_file=False):\n'
                  ' old_level = global_values.GAME_INSTANCE.levels[level_num]\n'
                  ' if old_level == global_values.GAME.INSTANCE.level:\n'
                  '  if regen_from_file:\n'
                  '   world_script.generate_generation_code(world_script=old_level.name, regen=true)\n'
                  '  else:\n'
                  '   code_to_append = world_script.generated_level_code[level_num]\n'
                  '   code_to_append = "{}{}".format(code_to_append, new_code)\n'
                  '   exec(code_to_append)\n'
                  '   world_script[level_num] = code_to_append\n'
                  '  global_values.GAME_INSTANCE.level = global_values.GAME_INSTANCE.levels[level_num]\n'
                  ' else:\n'
                  '  if regen_from_file:\n'
                  '   world_script.generate_generation_code(world_script=old_level.name, regen=true)\n'
                  '  else:\n'
                  '   code_to_append = world_script.generated_level_code[level_num]\n'
                  '   code_to_append = "{}{}".format(code_to_append, new_code)\n'
                  '   exec(code_to_append)\n'
                  '   world_script[level_num] = code_to_append\n'
                  '  global_values.GAME_INSTANCE.level = global_values.GAME_INSTANCE.levels[level_num]\n')

def tokenize(data):
    data = strip_comments(data)
    return data.replace('(', ' ( ').replace(')', ' ) ').split()

def read_from_tokens(tokens):
    if len(tokens) <= 0:
        raise SyntaxError('Unexpected EOF during parsing')
    if tokens[0] == ')':
        raise SyntaxError('Unexpected ) during parsing')
    elif tokens[0] == '(':
        tokens.pop(0)
        lisp_list = []
        while tokens[0] != ')':
            lisp_list.append(read_from_tokens(tokens))
        tokens.pop(0)
        return lisp_list
    else:
        return atomize(tokens.pop(0))

def atomize(token):
    if token.lower() == 't' or token.lower() == 'true':
        return 'True'
    elif token.lower() == 'f' or token.lower() == 'false': return 'False'
    try:
        return int(token)
    except ValueError:
        try:
            return float(token)
        except ValueError:
            return str(token)

def parse(data):
    return read_from_tokens(tokenize(data))

def make_procedure(name, args, body):
    a = ''
    for arg in zip(args):
        if a == '':
            a = arg[0]
        else:
            a = '{},{}'.format(a, lisp_eval(arg[0]))
    body = lisp_eval(body)
    return ('def {}({}):\n'
            ' {}\n').format(name, a, lisp_eval(body))

def environment():
    env = dict()
    env.update(vars(math))
    env.update({
        '+':'op.add', '-':'op.sub', '*':'op.mul', '/':'op.truediv', '//':'op.floordiv',
        'mod':'op.mod', 'exp':'op.pow', 'and':'op.and_', 'or':'op.or_', 'not':'op.not',
        '>':'op.gt', '<':'op.lt', '>=':'op.ge', '<=':'op.le',
        'eq':'op.eq',
        'isp':'op.is_',
        'nullp':'nullp',
        'cons':'cons',
        'car':'car',
        'cdr':'cdr',
        'print':'print'
    })
    return env

global_env = environment()
def python_reassembly(python_list):
    reassembly_code = ''
    previous_item = ''
    for item in python_list:
        if isinstance(item, list):
            reassembly_code = '{}({})'.format(reassembly_code, python_reassembly(item))
            try:
                reassembly_code = '{}{}'.format(reassembly_code, python_list[python_list.index(item)
                                                                             + 1].replace('\\n', '\n'))
                previous_item = python_list[python_list.index(item) + 1]
            except IndexError:
                pass
            reassembly_code = '{} '.format(reassembly_code)
        elif item == previous_item:
            pass
        else:
            try:
                reassembly_code = '{} {}'.format(reassembly_code, item.replace('\\n', '\n'))
            except AttributeError:
                reassembly_code = '{} {}'.format(reassembly_code, item)
    return reassembly_code

def string_reassembly(string_list):
    reassembled_string = ""
    string_list.pop(0)
    for item in string_list:
        reassembled_string = '{} {}'.format(reassembled_string, item)
    return reassembled_string

def lisp_eval(x, env=global_env):
    global inline_python
    if isinstance(x, str):
        if x == 'True' or x == "False":
            return x
        try:
            return env[x]
        except KeyError:
            return x
    elif isinstance(x, dict):
        return_string = 'if {}:\n {}\n'.format(lisp_eval(x['test']), lisp_eval(x['exp']))
        for key in x:
            if key == 'elif':
                return_string = '{}elif {}:\n'.format(return_string, lisp_eval(x[key][1]))
                return_string = '{} {}\n'.format(return_string, lisp_eval(x[key][2]))
        try:
            return_string = '{}else:\n'.format(return_string)
            return_string = '{} {}'.format(return_string, lisp_eval(x['else'][1]))
        except KeyError:
            pass
        return return_string
    elif not isinstance(x, list):
        return x
    elif x[0] == 'quit':
        return 'sys.exit(int({}))'.format(x[1])
    elif x[0] == 'exit':
        return 'sys.exit()'
    elif x[0] == 'live-patch-level':
        (_, level_number) = x
    elif x[0] == 'enable-python-macro':
        inline_python = True
    elif x[0] == 'disable-python-macro':
        inline_python = False
    elif x[0] == 'Python':
        if inline_python:
            reassembled_code = python_reassembly(x[1])
            return python_reassembly(x[1])
        else:
            print("INLINE PYTHON HAS BEEN DISABLED!")
            return ''
    elif x[0] == 'str':
        return string_reassembly(x)
    elif x[0] == 'if':
        if_structure = {}
        if_structure['test'] = x[1]
        if_structure['exp'] = x[2]
        i = 3
        while i < len(x):
            if len(x[i]) == 3:
                if_structure['elif'] = x[i]
            elif len(x[i]) == 2:
                if_structure['else'] = x[i]
            i += 1
        return lisp_eval(if_structure)
    elif x[0] == 'defunc':
        (_, name, params, body) = x
        return make_procedure(name, params, body)
    elif x[0] == 'defvar':
        (_, var) = x
        return '{} = None'.format(var)
    elif x[0] == 'delatom':
        (_, var) = x
        return 'del {}'.format(var)
    elif x[0] == 'setf':
        (_, var, exp) = x
        return '{} = {}'.format(var, lisp_eval(exp))
    elif x[0] == 'return':
        return 'return {}\n'.format(lisp_eval(x[1]))
    elif x[0] == 'while':
        return 'while {}:\n {}\n'.format(lisp_eval(x[1]), lisp_eval(x[2]))
    elif x[0] == 'for':
        return 'for {} in {}:\n {}\n'.format(x[1], x[2], lisp_eval(x[3]))
    elif x[0] == 'defgObject':
        if len(x[2]) < 1:
            x[2] = 'gObject'
        py_code = (lisp_eval(x[3]) + 't').strip(' ()t')
        return 'class {}({}):\n {}\ngame_basics.register_objects("{}", {})'.format(x[1], x[2][0], py_code, x[1], x[1])
    elif x[0] == 'defLevel':
        print("please do not use this!")
        return 'main.GAME_INSTANCE.append(level.Level())\n'
    elif 'del' in x[0]:
        return '{}("{}")'.format(x[0], x[1])
    elif 'super.' in x[0]:
        if x[0] == 'True' or x[0] == 'False':
            return x[0]
        proc = lisp_eval(x[0], env)
        passed_args = [lisp_eval(arg, env) for arg in x[1:]]
        args = ""
        for arg in passed_args:
            if args == '':
                args = arg
            else:
                args = '{},{}'.format(args, lisp_eval(arg))
        return ' super().{}({})'.format(x[0].split('super.')[1], args)
    else:
        if x[0] == 'True' or x[0] == 'False':
            return x[0]
        proc = lisp_eval(x[0], env)
        passed_args = [lisp_eval(arg, env) for arg in x[1:]]
        args = ""
        for arg in passed_args:
            if args == '':
                args = arg
            else:
                args = '{},{}'.format(args, lisp_eval(arg))
        return '{}({})'.format(proc, args)

def strip_comments(line):
    new_line = '{}\n'.format(line.split(';')[0])
    return new_line

def translate_line(line):
    py_prog = ''
    line = strip_comments(line)
    if line.strip() != '':
        py_prog = '{}{}\n'.format(py_prog, lisp_eval(parse(line)))
    return py_prog

def translate_program(program_file):
    py_prog = python_program
    with open(program_file, 'r') as f:
        for line in f:
            line = strip_comments(line)
            if line.strip() != '':
                py_prog = '{}{}\n'.format(py_prog, lisp_eval(parse(line.strip('\n'))))
    return py_prog

def _live_patch(pipe_to_main):
    try:
        while True:
            with open('./1.fifo', 'r') as feel_fifo:
                file_name = feel_fifo.read()
                if file_name.startswith('shutdown'):
                    break
                else:
                    pipe_to_main.send(file_name)
                    print('sent to compile')
    except InterruptedError:
        pass
    except KeyboardInterrupt:
        sys.exit()

def live_patch():
    global inline_python
    inline_python = True
    if not allow_livepatching:
       return 0
    try:
        if READ_PIPE.poll(0.0005):
            data = READ_PIPE.recv()
            with open('./.autogen-python/live_patch.py', 'w') as f:
                f.write('{}{}'.format(python_program, translate_program(data)))
            exec('{}{}'.format(python_program, translate_program(data)))
    except InterruptedError:
        pass
    except KeyboardInterrupt:
        sys.exit()

def cleanup():
    try:
        os.remove('./1.fifo')
        READ_PROCESS.terminate()
        READ_PROCESS.join()
    except FileNotFoundError:
        pass
    except AttributeError:
        pass
    except NameError:
        pass

def setup():
    try:
        os.remove('./1.fifo')
    except FileNotFoundError:
        pass
    global allow_livepatching
    if allow_livepatching:
        os.mkfifo('./1.fifo')
        global READ_PIPE, READ_PROCESS
        READ_PIPE, write_pipe = multiprocessing.Pipe(False)
        READ_PROCESS = multiprocessing.Process(target=_live_patch, args=(write_pipe,))
        READ_PROCESS.start()

def send_file(file_name):
    global allow_livepatching
    if allow_livepatching:
        with open('./1.fifo', 'w') as feel_fifo:
            feel_fifo.write(file_name)
            sys.exit('sent to read_process')

try:
    if sys.argv[1] == '--compile' or sys.argv[1] == '--live-patch':
        try:
            send_file(sys.argv[2])
        except IndexError:
            print("Please provide a filename!")
    elif sys.argv[1] == '--help':
        print("FEEL.py\n")
        print("--compile [file] or --live-patch [file]: allows live-patching/editing.")
        print("--help: Shows this display")
except IndexError:
    pass
