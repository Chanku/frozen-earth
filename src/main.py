#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import pygame
import intro_stuff
import level
import game_basics
import time
import world_script
import global_values

HEIGHT = global_values.HEIGHT
WIDTH = global_values.WIDTH
height = HEIGHT
width = WIDTH
def game_loop():
    pygame.init()
    h_w = (global_values.HEIGHT, global_values.WIDTH)
    is_running = True
    window = pygame.display.set_mode(h_w)
    pygame.display.set_caption(global_values.GAME_NAME)
    clock = pygame.time.Clock()
    game, g1, player, audio_stuff  = intro_stuff.intro_screen(window, clock)
    global_values.GAME_INSTANCE = game
    intro_stuff.main_menu(window, clock)
    fps = 20
    cheat_mode = False
    debug = global_values.DEBUG
    enter_once = False
    if debug and world_script.is_FEEL:
        import feel
        feel.setup()
    while is_running:
        if (not cheat_mode and not debug) or (cheat_mode and debug):
            pygame.display.set_caption(global_values.SECOND_GAME_NAME)
        elif debug:
            pygame.display.set_caption('{} --DEBUG--'.format(global_values.GAME_NAME))
        if debug and world_script.is_FEEL:
            feel.live_patch()
        is_running = game.is_running
        player.frame_advance()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False
            elif event.type == pygame.KEYDOWN:
                if pygame.key.name(event.key).lower() == 'up':
                    player.set_animation('up')
                    player.animation_string = 'up'
                    player.change_y -= player.speed
                elif pygame.key.name(event.key).lower() == 'down':
                    player.set_animation('down')
                    player.animation_string = 'down'
                    player.change_y += player.speed
                elif pygame.key.name(event.key).lower() == 'left':
                    player.set_animation('left')
                    player.animation_string = 'left'
                    player.change_x -= player.speed
                elif pygame.key.name(event.key).lower() == 'right':
                    player.set_animation('right')
                    player.change_x += player.speed
                    player.animation_string = 'right'
                elif pygame.key.name(event.key).lower() == 't':
                    if cheat_mode:
                        game.move_to_next = True
                        game.change_room()
                elif pygame.key.name(event.key).lower() == 'i':
                    if cheat_mode:
                        player.health = 200
                        player.damage += 200
                        player.reach += 600
                        player.speed = 10
                elif pygame.key.name(event.key).lower() == 'u':
                    if cheat_mode:
                        player.health = 10
                        player.reach -= 600
                        player.damage -= 200
                        player.speed = 5
                elif pygame.key.name(event.key).lower() == 'p':
                    time.sleep(120)
                elif pygame.key.name(event.key).lower() == 'return':
                    if enter_once and not cheat_mode:
                        print("CHEAT MODE ACTIVATED!")
                        print("ALSO QUIT FREAKING CHEATING!")
                        pygame.display.set_caption("{} --CHEAT MODE-- (You dirty cheater)".format(GAME_NAME))
                        cheat_mode = True
                        enter_once = False
                    elif enter_once and cheat_mode:
                        print("CHEAT MODE DEACTIVATED")
                        print("THANK-YOU FOR NOT CHEATING ANYMORE!")
                        pygame.display.set_caption(GAME_NAME)
                        cheat_mode = False
                        enter_once = False
                    elif not enter_once:
                        enter_once = True
                elif pygame.key.name(event.key).lower() == 'l':
                    if cheat_mode:
                        for item in game.level.grid.sprites():
                            if isinstance(item, game_basics.Enemy):
                                item.kill()
            elif event.type == pygame.KEYUP:
                player.audio = 0
                if pygame.key.name(event.key).lower() == 'up':
                    player.set_still('up')
                    player.animation_string = 's__up'
                    if player.change_y < 0:
                        player.change_y += player.speed
                elif pygame.key.name(event.key).lower()  == 'down':
                    player.set_still('down')
                    player.animation_string = 's__down'
                    if player.change_y > 0:
                        player.change_y -= player.speed
                elif pygame.key.name(event.key).lower() == 'left':
                    player.set_still('left')
                    player.animation_string = 's__left'
                    if player.change_x < 0:
                        player.change_x += player.speed
                elif pygame.key.name(event.key).lower() == 'right':
                    player.set_still('right')
                    player.animation_string = 's__right'
                    if player.change_x > 0:
                        player.change_x -= player.speed
                elif pygame.key.name(event.key).lower() == 'a':
                    game.attack = True
        window.fill((255,255,0))
        # player.update()
        game.update()
        game.change_room()
        g1.draw(window)
        pygame.display.flip()
        clock.tick(fps)
    pygame.quit()
    if debug and world_script.is_FEEL:
        feel.cleanup()

if __name__ == "__main__":
    game_loop()
