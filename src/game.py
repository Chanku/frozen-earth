#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import pygame
import level
import game_basics
import world_script

def create_levels(screen, player):
    return world_script.load_all(screen, player)
