#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import audio
import main
import random
import pygame
import sys
import time

obj_dictionary = {}
class gObject(pygame.sprite.Sprite):
    def __init__(self, image):
        super().__init__()
        images = {}
        for img in image:
            if 'strip' in img:
                images['ani_{}'.format(img.split('_')[1])] = pygame.image.load(img)
            elif len(img.split('_')) >= 2:
                if img.split('_')[1].endswith('.png'):
                    images['still_{}'.format(img.split('_')[1][:len(img.split('_')[1])-4])] = pygame.image.load(img)
                elif not img.split('_')[1].endswith('.png'):
                    images['still_{}'.format(img.split('_')[1])] = pygame.image.load(img)
            else:
                images['still_{}'.format(img[9:len(image)-5])] = pygame.image.load(img)

        for img_key in images:
            i = 0
            if 'ani_' in img_key:
                img = images[img_key]
                ls = []
                while i < img.get_width() / 32:
                    x = img.copy()
                    x.set_clip(pygame.Rect(i * 32, 0, 32, img.get_height()))
                    ls.append(x.subsurface(x.get_clip()))
                    i += 1
                images[img_key] = ls

        self.image_dict = images
        self.animation = []
        self.animation_string = 'none'
        try:
            self.image = self.image_dict['still_down']
        except KeyError:
            try:
                self.image = self.image_dict['still_{}'.format(image[0][9:][:len(image) - 5])]
            except KeyError:
                self.image = self.image_dict['still_{}'.format(image[0].split('_')[1][:len(image) - 6])]
        self.rect = self.image.get_rect()
        self.change_x = 0
        self.change_y = 0

    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def frame_advance(self):
        try:
            self.image = self.animation[self.animation.index(self.image) + 1]
        except ValueError:
            pass
        except IndexError:
            self.animation_restart()
        except KeyError:
            try:
                self.animation_restart()
            except IndexError:
                self.image = self.image

    def frame_reverse(self):
        try:
            self.image = self.animation[self.animation.index(self.image) - 1]
        except IndexError:
            self.image = self.animation[len(self.animation) - 1]

    def animation_restart(self):
        self.image = self.animation[0]

    def set_animation(self, key):
        self._set_animation('ani_{}'.format(key))

    def set_still(self, key):
        self._set_still('still_{}'.format(key))

    def _set_animation(self, key):
        self.animation = self.image_dict[key]
        self.image = self.animation[0]

    def _set_still(self, key):
        self.image = self.image_dict[key]
        self.animation = []

    def move(self):
        new_x = self.rect.x + self.change_x
        new_y = self.rect.y + self.change_y
        self.set_pos(new_x, new_y)

    def update(self):
        if self.change_x != 0 or self.change_y != 0:
            self.move()

    # def collision(self, checking_rect):
    #     if self.rect.colliderect(checking_rect):
    #         print('collision!')

class Mob(gObject):
    def __init__(self, x, y, image, health, damage, reach, speed, drops=None):
        super().__init__(image)
        self.x_change = 0
        self.y_change = 0
        self.drops = drops
        self.set_pos(x, y)
        self.reach = reach
        self.health = health
        self.max_health = health
        self.speed = speed
        self.collision_counter = 0
        self.awaken = True
        self.damage = damage

    def update(self):
        if self.awaken:
            super().update()
            self.check_for_death()

    def heal(self, healed_value):
        if self.health < self.max_health:
            self.health += healed_value
            return True
        return False

    def check_for_death(self):
        if self.health <= 0:
            self.kill()

class Item(gObject):
    def __init__(self, image, animation_speed=0):
        super().__init__(image)
        self.animation_speed = animation_speed

    def update(self):
        super().update()

class Player(Mob):
    def __init__(self, x=0, y=0, health=10, damage=4, reach=10, speed=5, inventory=[]):
        self.audio = 0
        self.inventory = inventory
        super().__init__(x, y,
                ['./assets/sprite.png', './assets/explorer_down.png',
                 './assets/explorer_down_strip8.png', './assets/explorer_up.png',
                 './assets/explorer_up_strip8.png', './assets/explorer_right.png',
                 './assets/explorer_right_strip8.png', './assets/explorer_left.png',
                 './assets/explorer_left_strip8.png'], health, damage, reach, speed, self.inventory)

    def update(self):
        super().update()
        if (self.change_x != 0 or self.change_y != 0) and not self.audio:
            # audio._sound['step'].play()
            self.audio += 1
        elif self.audio >= self.speed:
            self.audio = 0
        else:
            self.audio += 1
        if self.change_x < 0 and self.animation_string != 'left' and self.change_y == 0:
            self.set_animation('left')
            self.animation_string = 'left'
        elif self.change_x > 0 and self.animation_string != 'right' and self.change_y == 0:
            self.set_animation('right')
            self.animation_string = 'right'
        elif self.change_y < 0 and self.animation_string != 'up' and self.change_x == 0:
            self.set_animation('up')
            self.animation_string = 'up'
        elif self.change_y > 0 and self.animation_string != 'down' and self.change_x == 0:
            self.set_animation('down')
            self.animation_string = 'down'
        self.check_for_death()

    def check_for_death(self):
        if self.health <= 0:
            self.kill()
            # audio._sound['skeleton'].stop()
            # audio._sound['game_over'].play()
            # time.sleep(audio._sound['game_over'].get_length())
            sys.exit(0)

class Enemy(Mob):
    def __init__(self, x, y, image, health, damage, reach, speed, awaken=True, drops=None):
        super().__init__(x, y, image, health, damage, reach, speed, drops)
        self.awaken = awaken

    def update(self):
        super().update()

class Terrain(gObject):
    def __init__(self, image, collision=True):
        super().__init__(image)
        self.collision = collision

class Portal(Terrain):
    def __init__(self, image, x, y):
        super().__init__(image)
        # self.collision = True
        self.collision = False
        self.x_placement = x
        self.y_placement = y
    def move_mob(self, mob):
        mob.set_pos(self.x_placement, self.y_placement)

class Room_Exit(gObject):
    def __init__(self, image, room_movement_int = 1, x_set=None, y_set=None):
        super().__init__(image)
        self.room_movement = room_movement_int
        self.collision = False
        self.x_set = x_set
        self.y_set = y_set

    def move_mob(self, mob):
        if not self.x_set and not self.y_set:
            if self.rect.x == (main.WIDTH - self.rect.width) and self.rect.y >= 0:
                mob.set_pos((0 + self.rect.width), mob.rect.y)
            elif self.rect.x >= 0 and self.rect.y == (main.HEIGHT - self.rect.height):
                mob.set_pos(mob.rect.x, (0 + self.rect.height))
            elif self.rect.x == 0 and self.rect.y >= 0:
                mob.set_pos((main.WIDTH - 2*self.rect.width), mob.rect.y)
            elif self.rect.x >= 0 and self.rect.y == 0:
                mob.set_pos(mob.rect.x, (main.HEIGHT - 2*self.rect.height))
        else:
            mob.set_pos(self.x_set, self.y_set)

class Lock(Terrain):
    def __init__(self, image, str_key_value, kill_on_death=True, image_change_on_death=False):
        super().__init__(image)
        self.key_value = str_key_value
        self.kill_on_death = kill_on_death
        self.image_change_on_death = image_change_on_death

    def unlock(self, pstr_key_value):
        if self.key_value == pstr_key_value:
            if self.kill_on_death:
                self.kill()
            elif not self.kill_on_death and self.image_change_on_death:
                self.collision = False
                self._unlocked_image_change
            elif not self.kill_on_death and not self.image_change_on_death:
                _unlock()

    def _unlocked_image_chage(self):
        raise NotImplementedError

    def _unlock(self):
        raise NotImplementedError

class MagicBarrier(gObject):
    def __init__(self, image):
        super().__init__(image)

class Destructable_Container(Terrain):
    def __init__(self, image, contains):
        super().__init__(image)
        self.inventory = contains

    def open_container(self):
        self.kill()
        return self.inventory

class Standard_Container(Terrain):
    def __init__(self, image, contains):
        super().__init__(image)
        self.inventory = contains

    def open_container(self):
        self._change_to_open()
        return self.inventory

    def _change_to_open(self):
        raise NotImplementedError

class Player_Projectile(gObject):
    def __init__(self, image, damage=0, speed=2):
        super().__init__(image)
        self.damage = damage
        self.speed = speed

class Enemy_Projectile(gObject):
    def __init__(self, image, damage=0, speed=2):
        super().__init__(image)
        self.damage = damage
        self.speed = speed

def register_objects(Name, obj):
    obj_dictionary[Name] = obj
