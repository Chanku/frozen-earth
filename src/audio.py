#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import pygame
import os

_sound = {}
_music = []

def load_audio():
    _load_sound()
    _load_music()

def _load_sound():
    for item in os.listdir('./audio/sfx/'):
        _sound[item[:len(item) - 4]] = pygame.mixer.Sound('./audio/sfx/{}'.format(item))
        _sound[item[:len(item) - 4]].set_volume(0.2)
    # _sound['skeleton'].set_volume(0.4)
    # print(_sound)
    # _sound['laugh'].play()

def _load_music():
    pass
