# global GAME_NAME, SECOND_GAME_NAME, GAME_INSTANCE, DEBUG, HEIGHT, WIDTH
HEIGHT = 640
WIDTH = 640
height = HEIGHT
width = WIDTH
GAME_NAME = 'Frozen Earth Engine'
SECOND_GAME_NAME = GAME_NAME
GAME_INSTANCE = None
DEBUG = True
ALLOW_LIVEPATCHING = True
