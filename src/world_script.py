#Licensing for this file is dictated by the MIT/LGPLv2.1 for Non-Commercial works and the LGPL v2.1 for Commercial Works
import level
import game_basics
level_generation_code = []
is_FEEL = True
try:
    import feel
    import main
except ImportError:
    is_FEEL = False
import os
import sys
import importlib

block_delimeter = " | "
row_delimeter = "\n"
#
# World Script is a declarative programming language designed for the rapid creation
#  and development of worlds within the Frozen Earth Game Engine.
#
# World Script is designed to be used with FEEL. If FEEL is not installed it will allow
#  for a script for a room to contain an external python file with code relevant to the room.
#

# This actually loads the world only this should be explicitly called
def load_all(screen, player):
    return level.Game(read_gen(screen), screen, player)

def read_gen(screen):
    level_list = []
    for world_script in os.listdir('./scripts/world_scripts/'):
        python_file = ""
        if (world_script.endswith('.few') or (not world_script.endswith('.py')
            and not world_script.endswith('.py'))
           ) and not world_script.endswith('.swp') and 'pycache' not in world_script:
            block_delimeter = " | "
            row_delimeter = "\n"
            with open('./scripts/world_scripts/{}'.format(world_script), 'r') as script:
                for line_number, line in enumerate(script):
                    if line.startswith('!'): #This is the marking for setting a global script value
                        if line[1:].startswith('@'): #This is the marking for setting a read_value
                            if line[2:].startswith("ROW_DELIMITER "):
                                try:
                                    row_delimeter = line.split('ROW_DELIMITER ')[1]
                                    if row_delimeter == '!' or row_delimeter == "!@" or row_delimeter == "!!":
                                        raise IndexError
                                    elif row_delimeter == '!$':
                                        raise IndexError
                                except IndexError:
                                    print("ERROR: ROW DELIMITER INVALID!")
                                    print("FROM LINE {}:{}".format(str(line_number), line))
                                    sys.exit(0)
                            if line[2:].startswith("BLOCK_DELIMETER "):
                                try:
                                    block_delimeter = line.split('BLOCK_DELIMETER ')[1]
                                    if block_delimeter == '!' or row_delimeter == "!@" or row_delimeter == "!!":
                                        raise IndexError
                                    elif block_delimeter == '!$':
                                        raise IndexError
                                    elif block_delimeter == row_delimeter:
                                        raise IndexError
                                except IndexError:
                                    print("ERROR: BLOCK DELIMETER INVALID!")
                                    print("FROM LINE {}:{}".format(str(line_number), line))
                                    sys.exit(0)
                        elif line[1:].startswith('!') or line[1:].startswith('$'): #This is the marking for FEEL or python
                            if line[2:].startswith('EXTERNAL_PYTHON_FILE'):
                                split_line = line.split('EXTERNAL_PYTHON_FILE')
                                python_file = ""
                                try:
                                    if len(split_line[1].split('./')) > 1:
                                        print('l')
                                        raise IndexError
                                    elif len(split_line[1].split('../')) > 1:
                                        print('t')
                                        raise IndexError
                                    elif len(split_line) > 2:
                                        raise IndexError
                                    with open(('./scripts/world_scripts/{}'
                                              ).format(split_line[1].strip(' ').strip('\n'), 'r')
                                              ) as pyscript:
                                       python_file = pyscript.read()
                                except IndexError:
                                    print("ERROR: INVALID OR NO PYTHON FILE NAME GIVEN!")
                                    print("FROM LINE {}:{}".format(str(line_number), line))
                                    sys.exit(0)
                            elif line[2:].startswith('EXTERNAL_FEEL_FILE'):
                                try:
                                    if not is_FEEL:
                                        raise SyntaxError("FEEL IS NOT ON THIS SYSTEM")
                                except IndexError:
                                    raise SyntaxError("FEEL IS NOT ON THIS SYSTEM")
                                split_line = line.splti("EXTERNAL_FEEL_FILE")
                                FEEL_file = ""
                                try:
                                    if len(x.split('./')) > 1:
                                        raise SyntaxError("INVALID FLISP FILE NAME GIVEN")
                                    elif len(x.split('../')) > 1:
                                        raise SyntaxError("INVALID FLISP FILE NAME GIVEN")
                                    elif len(split_line) > 1:
                                        raise SyntaxError("INVALID FLISP FILE NAME GIVEN")
                                    FEEL_file = FEEL.compile_FEEL(split_line[1])
                                    python_file = feel.translate_program(split_line[1])
                                except IndexError:
                                    print("FROM LINE {}:{}".format(str(line_number), line))
                                    raise SyntaxError("NO FLISP NAME GIVEN")
                    else:
                        break

            with open('./scripts/world_scripts/{}'.format(world_script), 'r') as script:
                f = ""
                for line in script:
                    if not line.startswith("!"):
                        f = "{}{}".format(f, line)
            try:
                level_list.append(generate_generation_code(f, block_delimeter,
                                                           row_delimeter, screen, world_script,
                                                           python_file))
            except UnboundLocalError:
                level_list.append(generate_generation_code(f, block_delimeter,
                                                           row_delimeter, screen, world_script))
    print(level_list)
    return level_list

def strip_special_chars(world_script):
    with open('./scripts/world_scripts/{}'.format(world_script), 'r') as script:
        f = ""
        for line in script:
            if not line.startswith("!"):
                f = "{}{}".format(f, line)
    return f


#
# This code is designed to actually create the code responsible for the generation of a level.
#
def generate_generation_code(f='', new_block=block_delimeter, new_row=row_delimeter,
                             screen='', world_script='', previous_python = None, regen = False, level_num=0):
    generated_code = "import level\nimport pygame\nimport game_basics\n"
    if not previous_python:
        generated_code = ('{}x = None\ndef generate_level(screen):\n'
                          ' this_level = level.Level("{}")\n'
                         ).format(generated_code, world_script) #Replace the 600, 600 with the actual screen size as set
    elif previous_python:
        generated_code = ('{}x = None\ndef generate_level(screen):\n'
                          ' {}\n'
                          ' this_level = level.Level("{}")\n'
                         ).format(generated_code, previous_python, world_script)
    if f == '':
        f = strip_special_chars(world_script)
    blocks = []  #This will be a list of lists of strings. Where each list = one row
    block_rows = f.rstrip("\t").split(new_row)
    for block_row in block_rows:
        blocks.append(block_row.split(new_block))
    del block_rows
    row = 0
    for block_row in blocks:
        col = 0
        for block in block_row:
            if 'DELIMITER' not in block and '' != block.strip(' ').strip('\t').rstrip(' ').rstrip('\t'):
                if '(' in block and ')' in block:
                    generated_code = ('{} this_level.append(({}, {}), game_basics.obj_dictionary["{}"]({}))\n'
                                     ).format(generated_code, col * 32, row * 32,
                                              block.split('(')[0],
                                              block.split('(')[1].split(')')[0])
                else:
                    generated_code = ('{} this_level.append(({}, {}), game_basics.obj_dictionary["{}"]())\n'
                                     ).format(generated_code, col * 32, row * 32,
                                              block)
                col += 1
            elif " " in block.strip(' ').strip('\t').rstrip(' ').rstrip('\t') or '' in block.strip(' ').strip('\t').rstrip(' ').rstrip('\t'):
                col += 1
        row += 1
    generated_code = ('{} return this_level\n'
                      'global some_level\n'
                      'some_level = generate_level(screen)\n').format(generated_code)
    with open('./.autogen-python/{}.fee_auto.py'.format(world_script), 'w') as auto_file:
        auto_file.write(generated_code)
    exec(generated_code)
    # print(some_level)
    if not regen:
        level_generation_code.append(generated_code)
    else:
        level_generation_code[level_num] = generated_code
    return some_level

for item in os.listdir('./scripts/general_scripts/'):
    if item.endswith('.py') and "__init__" not in item:
        script = importlib.import_module('scripts.general_scripts.{}'.format(item[:len(item) - 3]))
        del script
    elif (item.endswith('.lisp') or item.endswith('.feel')):
        if is_FEEL:
            x = feel.translate_program('scripts/general_scripts/{}'.format(item))
            with open('./.autogen-python/{}.feel_auto.py'.format(item), 'w') as auto_file:
                auto_file.write(x)
            exec(x)
